from os import getcwd, path
from time import sleep
from dotenv import dotenv_values
from hmd import ads


config = dotenv_values(".env")
BASE = getcwd()
DRIVER = path.join(BASE, 'drivers', config['DRIVER'])
IPS = [
    '',
    '171.244.16.89'
]


if __name__ == '__main__':
    for ip in IPS:
        options = {}
        if ip:
            options = {
                'proxy': {
                    'https': f"https://{config['PROXY_USR']}:{config['PROXY_PWD']}@{ip}:{config['PROXY_PORT']}"
                }
            }

        link = 'https://hoadm.net/'
        ads(DRIVER, options, link)
        sleep(300)
