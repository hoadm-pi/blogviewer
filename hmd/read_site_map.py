import xml.etree.ElementTree as ET
from urllib.request import urlopen


def get_links(sitemap):
    links = []
    with urlopen(sitemap) as f:
        tree = ET.parse(f)
        root = tree.getroot()
        for child in root:
            links.append(child[0].text)

    return links
