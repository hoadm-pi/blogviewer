from seleniumwire import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep


def ads(driver, options, link):
    driver = webdriver.Firefox(executable_path=driver, seleniumwire_options=options)
    driver.get(link)

    try:
        element = WebDriverWait(driver, 30).until(
            EC.presence_of_element_located((By.CLASS_NAME, "google-auto-placed"))
        )

        if element:
            sleep(150)
            element.click()
            sleep(10)
    finally:
        driver.quit()
