from seleniumwire import webdriver
from time import sleep


def surf(driver, options, link, duration=30):
    interaction_no = 5
    break_time = int(duration) // interaction_no
    driver = webdriver.Firefox(executable_path=driver, seleniumwire_options=options)
    driver.get(link)
    for i in range(1, interaction_no + 1):
        driver.execute_script(f"window.scrollTo(0, document.body.scrollHeight / {interaction_no} * {i})")
        sleep(break_time)
    driver.close()
