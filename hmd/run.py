from . import *


def run(driver, ip, config):
    links = get_links(config['SITEMAP'])
    options = {}
    if ip:
        options = {
            'proxy': {
                'https': f"https://{config['PROXY_USR']}:{config['PROXY_PWD']}@{ip}:{config['PROXY_PORT']}"
            }
        }
    for link in links:
        surf(driver, options, link, config['DURATION'])
