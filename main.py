from os import getcwd, path
from dotenv import dotenv_values
from hmd import run


config = dotenv_values(".env")
BASE = getcwd()
DRIVER = path.join(BASE, 'drivers', config['DRIVER'])
IPS = [
    '',
    '171.244.16.89'
]


if __name__ == '__main__':
    for ip in IPS:
        run(DRIVER, ip, config)
